# Cut out silence in video / audio file

This python script uses ffmpeg to detect silence in a video file and remove these silent parts. 

## Required
ffmpeg installed on system and added to PATH

## Parameter / Arguments
| arguments | description |
| -------- | ------------|
| --input, -i | path to input file, is required |
| --minD, -d | minimum Duration (in seconds) of silence to detect, default 0.1 |
| --output, -o | path to store output file, default: same as input location |
| --outName, -n | specify name of output file, default: same as input + postfix '_Edited_NoSilence' |



## TODO
- estimated time / duration is not correct
- test with audio files and adapt to accept audio files as well
- make code faster (faster cutting / trimming)