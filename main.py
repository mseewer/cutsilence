import subprocess, sys, math, time, argparse
import os

def searchSilence(fileLocation, minDuration):
    print("Searching for silent parts . . .")
    with open("silenceDetectOutput.txt", mode='wb') as file:
        # docs: https://ffmpeg.org/ffmpeg-filters.html#silencedetect
        comand = "ffmpeg -i " + fileLocation +" -af silencedetect=n=-60dB:d="+str(minDuration)+" -f null - "
        output = subprocess.run(comand, stderr=subprocess.PIPE).stderr
        file.write(output)

def getSilenceTimestamps():
    timestamps = []
    with open(file="silenceDetectOutput.txt", mode='r') as file:
        lines = file.readlines()
        for line in lines:
            if line.startswith("[silencedetect @"):
                if line.find("silence_start") != -1:
                    match = line.split(":")[-1]
                    timestamps.append([float(match)])
                elif line.find("silence_end") != -1:
                    match = line.split("|")[0].split(":")[-1]
                    timestamps[-1].append(float(match))

    for i in range(len(timestamps)-1, 0, -1):
        times = timestamps[i]
        if (times[0] > times[1]):
            # never happend so far
            print(str(i) + " not good, internal:")
            print(times)
            sys.exit(1)
        if (times[0] == times[1]): #same start / end for silence => can be discarded
            timestamps.pop(i)
    


    print("Cleaning up timestamps.")
    for i in range(len(timestamps)-1, 1, -1):
        if (timestamps[i-1][1] >= timestamps[i][0]):
            newTime = [timestamps[i-1][0], timestamps[i][1]]
            timestamps.pop(i-1)
            timestamps.pop(i)
            timestamps.insert(i-1, newTime)
    return timestamps

def cuttingFile(fileLocation, fileEnding, timestamps):
    print("Starting extracting, this will take approximately: " + str(math.ceil(len(timestamps) / 200 )) + " minutes.")
    print("Finished:")
    print("0 %")
    start_time = time.time()
    comand = 'ffmpeg -y -i ' + fileLocation + ' -filter_complex "' #-y is to force overwirting existing files
    lastI = 1
    countTempFiles = 0
    for i in range(1, len(timestamps)):
        videotrim = "[0:v]trim=start=" + str(timestamps[i-1][1]) + ":end=" +str(timestamps[i][0]) + ",setpts=PTS-STARTPTS["+ str(i) +"v];"
        audiotrim = "[0:a]atrim=start=" + str(timestamps[i-1][1]) + ":end=" +str(timestamps[i][0]) + ",asetpts=PTS-STARTPTS["+ str(i) +"a];"

        comand += videotrim + audiotrim

        if (i % 200 == 0):
            add = ""
            for j in range(i-199, i+1):
                add = add + "[{}v][{}a]".format(str(j), str(j))
            
            comand = comand + add + 'concat=n=200":v=1:a=1[outv][outa] -map [outv] -map [outa] out' + str(countTempFiles) + '.' + str(fileEnding)
            subprocess.run(comand, capture_output=True)
            comand = 'ffmpeg -y -i ' + fileLocation + ' -filter_complex "'
            lastI = i+1
            countTempFiles += 1
            print(str( int( 100*(countTempFiles*200)/(len(timestamps)-1) ) ) + " %" )

    if (len(timestamps) - lastI) > 0 :
        add = ""
        for j in range(lastI, len(timestamps)):
            add = add + "[{}v][{}a]".format(str(j), str(j))
        comand = comand + add + "concat=n=" + str(len(timestamps) - lastI) + '":v=1:a=1[outv][outa] -map [outv] -map [outa]  out' + str(countTempFiles) + '.' + str(fileEnding)
        countTempFiles += 1
        output = subprocess.run(comand, capture_output=True)
        print("100 %")
    elapsed_time = time.time() - start_time
    print("Actual time needed: " + str(int(elapsed_time/60)) + " minutes and " + str(int(elapsed_time%60)) + " seconds")

    return countTempFiles # = how many temp files created

def combiningToOutput(countTempFiles, output, fileEnding):
    print("Combining to output file.")
    with open ("mylist.txt", mode='w') as f: 
        for i in range(countTempFiles):
            f.write("file 'out" + str(i) + "." + str(fileEnding) + "' \n")
    comand = "ffmpeg -y -f concat -safe 0 -i mylist.txt -c copy " +str(output) + "." + str(fileEnding)
    subprocess.run(comand, capture_output=True)


def cleanUp(countTempFiles, fileEnding):
    print("Cleaning up...")
    # subprocess.run : use shell=True when using a os shell command, like removing file
    for i in range(countTempFiles):
        comand = "del out"+str(i)+"." + str(fileEnding)
        subprocess.run(comand, shell=True)
    subprocess.run("del mylist.txt", shell=True)
    subprocess.run("del silenceDetectOutput.txt", shell=True)





if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="path to input file", required=True, type=str)
    parser.add_argument("--minD", "-d", help="minimum Duration (in seconds) of silence to detect, default 0.1.", default=0.1, type=float)
    parser.add_argument("--output", "-o", help="path to store output file, default: same as input location", type=str)
    parser.add_argument("--outName", "-n", help="specify name of output file, default: same as input + postfix '_Edited_NoSilence'", type=str)
    args = parser.parse_args()

    inputFile = args.input
    minDuration = args.minD
    outLocation = args.output
    outName = args.outName

    if not os.path.exists(inputFile):
        print("File not found!", file=sys.stderr)
        sys.exit(1)

    if outName: #check if outName is a valid one
        for char in ['/', '?', '<', '>', '\\', ':', '*', '|', '"']:
            if char in outName:
                print("Illegal character in name of output file! Do not use: " + char, file=sys.stderr)
                sys.exit(1)
    
    if minDuration == 0:
        print("WARNING: Duration of 0 is very aggressive and may produces not a good result!")
        answer = input("Continue with duration set to 0? ")
        if answer not in ["yes", "Yes", 'y', 'Y']:
            sys.exit(0)

    if "/" in inputFile:
        file = inputFile.rsplit("/", 1)[1] 
        location = inputFile.rsplit("/", 1)[0] + "\\"
    elif "\\" in inputFile:
        file = inputFile.rsplit("\\", 1)[1] 
        location = inputFile.rsplit("\\", 1)[0]  + "\\"
    else:
        file = inputFile
        location = ""

    if not outName:
        outName = file.rsplit(".", 1)[0] + "_Edited_NoSilence"
    if not outLocation:
        outLocation = location

    fileEnding = file.split(".")[-1]
    outputFile = outLocation + outName



    searchSilence(fileLocation=inputFile, minDuration=minDuration)
    timestamps = getSilenceTimestamps()
    countTempFiles = cuttingFile(fileLocation=inputFile, fileEnding=fileEnding, timestamps=timestamps)
    combiningToOutput(countTempFiles=countTempFiles, output=outputFile, fileEnding=fileEnding)
    cleanUp(countTempFiles=countTempFiles, fileEnding=fileEnding)
    print("Finished! Have fun.")